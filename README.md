# Subc

`subc` is a nice little command line utility that parses a
subset of the C Programming language.

## Installation

Make a virtualenv, like so, `virtuelenv --python=python3 .venv`.
Then you can can activate it using `source .vev/bin/activate`,
which will activate your virtualenv.

Once this is complete, go ahead and install the package through
the following command:

    python setup.py install

## Usage

For assignment 1:

    subc l <name of input file>

For assignment 2:

    subc y <name of input file>

## Example

![Show](./show.png)

## Caveats

There might be warnings when you run the `subc` executable.
Ignore them for now.