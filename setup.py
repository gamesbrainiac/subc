from setuptools import setup, find_packages

setup(
    name='subc',
    version='0.0.6',
    packages=find_packages(),
    url='https://bitbucket.org/gamesbrainiac/subc/',
    license='MIT',
    author='Qauzi Nafiul Islam',
    author_email='gamesbrainiac@gmail.com',
    description='A subc parser',
    long_description=open("README.md").read(),
    install_requires=[
        "ply>=3.8"
    ],
    entry_points={
        "console_scripts": [
            'subc=subc.subc:main'
        ]
    }
)
