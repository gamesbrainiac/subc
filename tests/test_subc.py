# encoding=utf-8

import unittest

from subc.subclex import lexer
from subc.subcyacc import parser

from subc.utils import tokens


class LexTestCase(unittest.TestCase):
    def get_new_lexer(self):
        return lexer.clone()

    def test_id(self):

        lxr = self.get_new_lexer()
        lxr.input("a")

        for tkn in tokens(lxr):
            if tkn.type == "ID":
                self.assertEqual(tkn.value, "a")

    def test_reserved(self):

        lxr = self.get_new_lexer()
        lxr.input("if a == b")

        for tkn in tokens(lxr):
            if tkn.type == "IF":
                self.assertEqual(tkn.value, "if")


class YaccTestCase(unittest.TestCase):

    def get_new_lexer(self):
        return lexer.clone()

    def test_simple(self):
        with open('./simple.txt') as f:
            s = f.read()
            lxr = self.get_new_lexer()
            lxr.input(s)

            for tkn in tokens(lxr):
                if tkn.type == "ID":
                    print(tkn.value)

            parser.parse(s)


if __name__ == '__main__':
    unittest.main()
