# encoding=utf-8

import sys
from .subclex import lex_runner
from .subcyacc import yacc_runner


def main():
    _, t, f = sys.argv

    if t == "l":
        lex_runner(f)
    else:
        yacc_runner(f)
