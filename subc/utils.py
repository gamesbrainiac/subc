# encoding=utf-8

def tokens(l):
    while True:
        tok = l.token()
        if tok:
            yield tok
        else:
            break