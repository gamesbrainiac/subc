# encoding=utf-8
import os

import sys

from ply import lex

reserved_tokens = {
    'if': 'IF',
    "else": "ELSE",
    "while": "WHILE",
    "for": "FOR",
    "repeat": "REPEAT",
    "until": "UNTIL",
    "return": "RETURN",
    "main": "MAIN"
}

normal_tokens = ["ID", "NUM", "RELOP", "ADDOP", "MULOP",
                 "ASSIGNOP", "NOT", "RPAREN", "CLOSEBRACE",
                 "LPAREN", "OPENBRACE", "SEMICOLON"]

tokens = normal_tokens + list(reserved_tokens.values())

t_NUM = r'\d+(\.\d+)?'
t_RELOP = r'<|>|<=|>=|==|!='
t_ADDOP = r'\+|\-|(\|\|)'
t_MULOP = r'\*|/|&&|%'
t_ASSIGNOP = r'='
t_NOT = r'!'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_OPENBRACE = r'\['
t_CLOSEBRACE = r'\]'
t_SEMICOLON = r';'
t_ignore = ' \t\n'


def t_error(t):
    # pass
    print("Illegal Character '{}' on line {}".format(t.value, t.lineno))
    t.lexer.skip(1)


def t_ID(t):
    r'[a-zA-Z][a-zA-Z0-9]{0,4}'
    t.type = reserved_tokens.get(t.value, 'ID')
    return t


lexer = lex.lex()


def lex_runner(fname):

    try:
        if os.path.exists(os.path.abspath(fname)):
            with open(fname) as f:
                lexer.input(f.read())

            table = set()
            while True:
                tok = lexer.token()
                if not tok:
                    break
                else:
                    if tok.type == "ID":
                        if tok.value not in table:
                            table.add(tok.value)
                        else:
                            continue
                print("{} : {}".format(tok.value, tok.type))
        else:
            print("Invalid file", file=sys.stderr)
    except IndexError:
        print("Invalid Arguments")
        exit()

if __name__ == '__main__':
    lex_runner(sys.argv[-1])
