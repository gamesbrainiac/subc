# encoding=utf-8
import sys

import ply.yacc as yacc

from .subclex import tokens, lexer
from .utils import tokens as get_tokens


def p_statementlist(p):
    """statementlist : statement SEMICOLON
                     | statementlist statement SEMICOLON"""

    if len(p) == 3:
        print("Stmt_list -> Stmt;")
    if len(p) == 4:
        print("Stmt_list -> Stmt_list Stmt;")


def p_statement(p):
    """statement : variable ASSIGNOP expression"""
    print("Stmt -> Variable ASSIGNOP expression")


def p_variable(p):
    """variable : ID
                | ID OPENBRACE expression CLOSEBRACE"""
    if len(p) == 2:
        print("Variable -> ID")
    else:
        print("Variable -> ID[Expression]")


def p_expression(p):
    """expression : simpleexpression
                  | simpleexpression RELOP simpleexpression"""
    if len(p) == 2:
        print("Expression -> Simple_expression")
    else:
        print("Expression -> Simple_expression REPLOP Simple_expression")


def p_simpleexpression(p):
    """simpleexpression : term
                        | simpleexpression ADDOP term"""
    if len(p) == 2:
        print("Simple_expression -> Term")
    else:
        print("Simple_expression -> Simple_expression ADDOP Term")


def p_term(p):
    """term : factor
            | term MULOP factor"""
    if len(p) == 2:
        print("Term -> Factor")
    else:
        print("Term -> Term MULOP Factor")


def p_factor(p):
    """factor : ID
              | NUM
              | LPAREN expression RPAREN
              | ID OPENBRACE expression CLOSEBRACE
              | NOT factor"""
    if len(p) == 2:
        if p[1].isnumeric():
            print("Factor -> NUM")
        else:
            print("Factor -> ID")
    if len(p) == 3:
        print("Factor -> NOT Factor")
    if len(p) == 4:
        print("Factor -> (Expression)")
    if len(p) == 5:
        print("Factor -> ID[Expression]")


def p_error(p):
    print(">> Syntax Error")

parser = yacc.yacc()


def yacc_runner(fname):
    try:
        with open(fname) as f:
            s = f.read()
    except IndexError:
        raise Exception("Invalid File.")

    lxr = lexer.clone()
    lxr.input(s)

    seen = set()
    for tkn in get_tokens(lxr):
        if tkn.type == "ID" and tkn.value not in seen:
            print(tkn.value)
            seen.add(tkn.value)

    parser.parse(s)

if __name__ == '__main__':
    yacc_runner(sys.argv[-1])
